module Translations exposing (..)

type Lang
  =  En
  |  Sk

getLnFromCode: String -> Lang
getLnFromCode code =
   case code of 
      "en" -> En
      "sk" -> Sk
      _ -> En

title: Lang -> String
title lang  =
  case lang of 
      En -> "Malinovo Cup"
      Sk -> "Malinový Pohár"

navHome: Lang -> String
navHome lang  =
  case lang of 
      En -> "Home"
      Sk -> "Domov"

navAbout: Lang -> String
navAbout lang  =
  case lang of 
      En -> "About run"
      Sk -> "O behu"

navRegistration: Lang -> String
navRegistration lang  =
  case lang of 
      En -> "Registration"
      Sk -> "Registrácia"

navPropositions: Lang -> String
navPropositions lang  =
  case lang of 
      En -> "Propozitions"
      Sk -> "Propozície"

navZeroWaste: Lang -> String
navZeroWaste lang  =
  case lang of 
      En -> "Zero Waste"
      Sk -> "Zero Waste"

navResults: Lang -> String
navResults lang  =
  case lang of 
      En -> "Results"
      Sk -> "Výsledky"

formSuccessMessage: Lang -> String
formSuccessMessage lang  =
  case lang of 
      En -> "Registration successful"
      Sk -> "Registrácia sa podarila"

formFailureMessage: Lang -> String
formFailureMessage lang  =
  case lang of 
      En -> "Registration failed"
      Sk -> "Registrácia sa podarila"

formFirstName: Lang -> String
formFirstName lang  =
  case lang of 
      En -> "Name"
      Sk -> "Meno"

formLastname: Lang -> String
formLastname lang  =
  case lang of 
      En -> "Surname"
      Sk -> "Priezvisko"

formTeam: Lang -> String
formTeam lang  =
  case lang of 
      En -> "Team"
      Sk -> "Tím"

formStartNumber: Lang -> String
formStartNumber lang  =
  case lang of 
      En -> "Start number"
      Sk -> "Štartovné prvočíslo"

formSubmit: Lang -> String
formSubmit lang  =
  case lang of 
      En -> "Register me!"
      Sk -> "Registruj ma!"

mainAbout: Lang -> String
mainAbout lang  =
  case lang of 
      En -> "O behu"
      Sk -> "O behu"

mainDate: Lang -> String
mainDate lang  =
  case lang of 
      En -> "Malinový pohár sa uskutoční "
      Sk -> "Malinový pohár sa uskutoční "

mainP1: Lang -> String
mainP1 lang  =
  case lang of 
      En -> "Pôvodná a už pomaly tradičná trať bude mať tentokrát až 6km (podrobne si ju môžete naštudovať z mapky) a ako doposiaľ sa tiahne cez anglický park a ďalej malebným lužným prostredím okolo ramena Malého Dunaja. Štartuje            a končí sa priamo pred Apponyiho kaštieľom, kde sa po našej cieľovej rovinke prechádzal v roku 1910 Theodore            Roosevelt.            Dlhoočakávanou novinkou je sľubovaná nová, 11km dlhá trať, ktorou sa môžete vydať do najkrajších zákutí Malinova!"
      Sk -> "Pôvodná a už pomaly tradičná trať bude mať tentokrát až 6km (podrobne si ju môžete naštudovať z mapky) a ako doposiaľ sa tiahne cez anglický park a ďalej malebným lužným prostredím okolo ramena Malého Dunaja. Štartuje            a končí sa priamo pred Apponyiho kaštieľom, kde sa po našej cieľovej rovinke prechádzal v roku 1910 Theodore            Roosevelt.            Dlhoočakávanou novinkou je sľubovaná nová, 11km dlhá trať, ktorou sa môžete vydať do najkrajších zákutí Malinova!"

mainP2: Lang -> String
mainP2 lang  =
  case lang of 
      En -> "Kategórie ženy a muži sú od tohto ročníka obohatené o novú premiérovú kategóriu Nordic walking."
      Sk -> "Kategórie ženy a muži sú od tohto ročníka obohatené o novú premiérovú kategóriu Nordic walking."

mainP3: Lang -> String
mainP3 lang  =
  case lang of 
      En -> "Deti sa ako vždy môžu tešiť na MINIbeh okolo kaštieľa (600m), za ktorého zvládnutie ich neminie zdravá odmena."
      Sk -> "Deti sa ako vždy môžu tešiť na MINIbeh okolo kaštieľa (600m), za ktorého zvládnutie ich neminie zdravá odmena."

mainTrack: Lang -> String
mainTrack lang  =
  case lang of 
      En -> "trať"
      Sk -> "trať"

registrationTitle: Lang -> String
registrationTitle lang  =
  case lang of 
      En -> "Registrácia"
      Sk -> "Registrácia"

registrationIntro: Lang -> String
registrationIntro lang  =
  case lang of 
      En -> "Tu sa registrujte. Štartovné je 6€ (deti 4€) prevodom na účet (nezabudnite pridať variabilný symbol, ktorý vám vygenerujeme) alebo 8€ na mieste. Po zaplatení vám pošleme email o tom, ako veľmi sa na vás tešíme a naštarte stačí len ak nám poviete vaše meno."
      Sk -> "Tu sa registrujte. Štartovné je 6€ (deti 4€) prevodom na účet (nezabudnite pridať variabilný symbol, ktorý vám vygenerujeme) alebo 8€ na mieste. Po zaplatení vám pošleme email o tom, ako veľmi sa na vás tešíme a naštarte stačí len ak nám poviete vaše meno."

registrationSrdcia1: Lang -> String
registrationSrdcia1 lang  =
  case lang of 
      En -> "Svojou účasťou na Malinovom pohári podporujete nadáciu "
      Sk -> "Svojou účasťou na Malinovom pohári podporujete nadáciu "

registrationStdcia2: Lang -> String
registrationStdcia2 lang  =
  case lang of 
      En -> ", ktorá pomáha deťom s chorými srdiečkami."
      Sk -> ", ktorá pomáha deťom s chorými srdiečkami."

registrationPrimes1: Lang -> String
registrationPrimes1 lang  =
  case lang of 
      En -> "Keďže jeden z organizátorov je poznačený matfyzom, namiesto štartovných čísel budete bežať so štartovnými prvočíslami. Ak si na žiadne nespomínate, "
      Sk -> "Keďže jeden z organizátorov je poznačený matfyzom, namiesto štartovných čísel budete bežať so štartovnými prvočíslami. Ak si na žiadne nespomínate, "

registrationPrimes2: Lang -> String
registrationPrimes2 lang  =
  case lang of 
      En -> "tu"
      Sk -> "tu"

registrationPrimes3: Lang -> String
registrationPrimes3 lang  =
  case lang of 
      En -> "sa môžete nechať inšpirovať. (alebo len nechajte prázdnu kolónku a my pre vás dáke nájdeme)"
      Sk -> "sa môžete nechať inšpirovať. (alebo len nechajte prázdnu kolónku a my pre vás dáke nájdeme)"

zeroWasteWhy: Lang -> String
zeroWasteWhy lang  =
  case lang of 
      En -> "Chceme ukázať, že bežecké podujatie sa dá organizovať aj inak ako s hŕbou zbytočných igelitiek, pohárikov, papierikov... proste odpadu, ktorý by nemusel vznikať, ak nastane vzájomná spolupráca nás všetkých. Sme si vedomí, že v dnešných podmienkach je vytváranie nulového odpadu skoro nemožné, ale robíme všetko pre to, aby sme ho vyprodukovali čo najmenej."
      Sk -> "Chceme ukázať, že bežecké podujatie sa dá organizovať aj inak ako s hŕbou zbytočných igelitiek, pohárikov, papierikov... proste odpadu, ktorý by nemusel vznikať, ak nastane vzájomná spolupráca nás všetkých. Sme si vedomí, že v dnešných podmienkach je vytváranie nulového odpadu skoro nemožné, ale robíme všetko pre to, aby sme ho vyprodukovali čo najmenej."

zeroWasteMotivation: Lang -> String
zeroWasteMotivation lang  =
  case lang of 
      En -> "Už od nultého ročníka organizujeme naše preteky tak, aby mali čo najmenší dopad na miesto, v ktorom žijeme a tejto myšlienky sa plánujeme držať aj po ďalšie ročníky. Radi privítame aj vašu pomoc či tipy a skúsenosti, ako to spraviť ešte lepšie. Skúsme to spolu!"
      Sk -> "Už od nultého ročníka organizujeme naše preteky tak, aby mali čo najmenší dopad na miesto, v ktorom žijeme a tejto myšlienky sa plánujeme držať aj po ďalšie ročníky. Radi privítame aj vašu pomoc či tipy a skúsenosti, ako to spraviť ešte lepšie. Skúsme to spolu!"

zeroWasteStatsTitle: Lang -> String
zeroWasteStatsTitle lang  =
  case lang of 
      En -> "Minuloročné štatistky"
      Sk -> "Minuloročné štatistky"

zeroWasteStatsP1: Lang -> String
zeroWasteStatsP1 lang  =
  case lang of 
      En -> "Minulý ročník sa nám podarilo na výrobu štartovných prvočísiel zrecyklovať 59 kartónových krabíc."
      Sk -> "Minulý ročník sa nám podarilo na výrobu štartovných prvočísiel zrecyklovať 59 kartónových krabíc."

zeroWasteStatsP2: Lang -> String
zeroWasteStatsP2 lang  =
  case lang of 
      En -> "Na výrobu štartového oblúka sme použili pneumatiky a obkladačky z čiernej skládky a drevené koly zo starej terasovej konštrukcie."
      Sk -> "Na výrobu štartového oblúka sme použili pneumatiky a obkladačky z čiernej skládky a drevené koly zo starej terasovej konštrukcie."

zeroWasteStatsP3: Lang -> String
zeroWasteStatsP3 lang  =
  case lang of 
      En -> "Jednorazové značenie trate sme nahradili pestrofarebnými vyradenými ponožkami od našich kamarátov."
      Sk -> "Jednorazové značenie trate sme nahradili pestrofarebnými vyradenými ponožkami od našich kamarátov."

zeroWasteStatsP4: Lang -> String
zeroWasteStatsP4 lang  =
  case lang of 
      En -> "Namiesto nepotrebných igelitových balíkov dostali víťazi sklenený pohár plný malín z lokálnej produkcie."
      Sk -> "Namiesto nepotrebných igelitových balíkov dostali víťazi sklenený pohár plný malín z lokálnej produkcie."

zeroWasteStatsP5: Lang -> String
zeroWasteStatsP5 lang  =
  case lang of 
      En -> "Počas samotných pretekov sme aj vďaka Vám produkovali iba bio odpad pozostávajúci z ohryzkov jabĺk a kôstok broskýň a sliviek."
      Sk -> "Počas samotných pretekov sme aj vďaka Vám produkovali iba bio odpad pozostávajúci z ohryzkov jabĺk a kôstok broskýň a sliviek."

zeroWasteTodoTitle: Lang -> String
zeroWasteTodoTitle lang  =
  case lang of 
      En -> "Čo môžem ako bežec urobiť / radšej neurobiť?"
      Sk -> "Čo môžem ako bežec urobiť / radšej neurobiť?"

zeroWasteTodoP1: Lang -> String
zeroWasteTodoP1 lang  =
  case lang of 
      En -> "prinesiem si vlastný pohár na iontový nápoj (Jasné, že nie jednorazový. Ale porcelánovú čašu zo                    súpravy, čo vaša babka dostala ako svadobný dar, radšej nechajte vo vitríne.)"
      Sk -> "prinesiem si vlastný pohár na iontový nápoj (Jasné, že nie jednorazový. Ale porcelánovú čašu zo                    súpravy, čo vaša babka dostala ako svadobný dar, radšej nechajte vo vitríne.)"

zeroWasteTodoP2: Lang -> String
zeroWasteTodoP2 lang  =
  case lang of 
      En -> "neprinesiem si pitie v PET fľaši"
      Sk -> "neprinesiem si pitie v PET fľaši"

zeroWasteTodoP3: Lang -> String
zeroWasteTodoP3 lang  =
  case lang of 
      En -> "neprídem sám autom (nechám sa zviesť, niekoho ešte zveziem, skúsim bus...)"
      Sk -> "neprídem sám autom (nechám sa zviesť, niekoho ešte zveziem, skúsim bus...)"

zeroWasteTodoP4: Lang -> String
zeroWasteTodoP4 lang  =
  case lang of 
      En -> "budem sa tešiť, že od organizátorov nedostanem igelitku plnú zbytočností..."
      Sk -> "budem sa tešiť, že od organizátorov nedostanem igelitku plnú zbytočností..."

zeroWasteTodoP5: Lang -> String
zeroWasteTodoP5 lang  =
  case lang of 
      En -> "čo len chcem a uznám za vhodné (Blysnite sa, inšpirujte nás, buďte frajeri!)"
      Sk -> "čo len chcem a uznám za vhodné (Blysnite sa, inšpirujte nás, buďte frajeri!)"

partnersTitle: Lang -> String
partnersTitle lang  =
  case lang of 
      En -> "Partneri"
      Sk -> "Partneri"

partnersP1: Lang -> String
partnersP1 lang  =
  case lang of 
      En -> "Chceš tu byť aj ty?"
      Sk -> "Chceš tu byť aj ty?"

partnersP2: Lang -> String
partnersP2 lang  =
  case lang of 
      En -> "Ozvi sa nám!"
      Sk -> "Ozvi sa nám!"

resultsTitle: Lang -> String
resultsTitle lang  =
  case lang of 
      En -> "Výsledky"
      Sk -> "Výsledky"

resultsSubttile: Lang -> String
resultsSubttile lang  =
  case lang of 
      En -> "Tohoročné výsledky"
      Sk -> "Tohoročné výsledky"

resultsRecords: Lang -> String
resultsRecords lang  =
  case lang of 
      En -> "Rekordy Trate"
      Sk -> "Rekordy Trate"

resultsWomanCategory: Lang -> String
resultsWomanCategory lang  =
  case lang of 
      En -> "Kategória Ženy"
      Sk -> "Kategória Ženy"

resultsWomanTime: Lang -> String
resultsWomanTime lang  =
  case lang of 
      En -> "s číslom 383 v roku 2020 a s doposiaľ neprekonaným časom"
      Sk -> "s číslom 383 v roku 2020 a s doposiaľ neprekonaným časom"

resultsManCategory: Lang -> String
resultsManCategory lang  =
  case lang of 
      En -> "Kategória Muži"
      Sk -> "Kategória Muži"

resultsManTime: Lang -> String
resultsManTime lang  =
  case lang of 
      En -> "s číslom 337 v roku 2019 a s doposiaľ neprekonaným časom "
      Sk -> "s číslom 337 v roku 2019 a s doposiaľ neprekonaným časom "

usTitle: Lang -> String
usTitle lang  =
  case lang of 
      En -> "O nás"
      Sk -> "O nás"

usHeno: Lang -> String
usHeno lang  =
  case lang of 
      En -> "Behajúci baleťák. Ak nie je práve zatvorený na baletnej sále na VŠMU, môžete ho zazrieť na divadelných                doskách alebo priamo za domom, v divokých Malinovských chotároch. Práve tam sa raz zrodila myšlienka                združiť všetkých behuchtivých do jednej veľkej malinovskej rodiny a spolu si zmerať sily!"
      Sk -> "Behajúci baleťák. Ak nie je práve zatvorený na baletnej sále na VŠMU, môžete ho zazrieť na divadelných                doskách alebo priamo za domom, v divokých Malinovských chotároch. Práve tam sa raz zrodila myšlienka                združiť všetkých behuchtivých do jednej veľkej malinovskej rodiny a spolu si zmerať sily!"

usMatus: Lang -> String
usMatus lang  =
  case lang of 
      En -> "Maratónsky matfyzák. Ak sa práve nepokúša testovať možnosti svojho tela na ultrapodujatiach alebo                prechodoch škótskou či balkánskou divočinou, tak rád derivuje kopce ako konštanty. Rukami behá po                klávesnici a programuje našu stránku, nohami behá po Malinove a organizuje."
      Sk -> "Maratónsky matfyzák. Ak sa práve nepokúša testovať možnosti svojho tela na ultrapodujatiach alebo                prechodoch škótskou či balkánskou divočinou, tak rád derivuje kopce ako konštanty. Rukami behá po                klávesnici a programuje našu stránku, nohami behá po Malinove a organizuje."

resultsViewPageMap: Lang -> String
resultsViewPageMap lang  =
  case lang of 
      En -> "Mapa stránky"
      Sk -> "Mapa stránky"

resultsViewW11km: Lang -> String
resultsViewW11km lang  =
  case lang of 
      En -> "Výsledky '21 - 11000m Ženy"
      Sk -> "Výsledky '21 - 11000m Ženy"

resultsViewM11km: Lang -> String
resultsViewM11km lang  =
  case lang of 
      En -> "Výsledky '21 - 11000m Muži"
      Sk -> "Výsledky '21 - 11000m Muži"

resultsViewW6km: Lang -> String
resultsViewW6km lang  =
  case lang of 
      En -> "Výsledky '21 - 6000m Ženy"
      Sk -> "Výsledky '21 - 6000m Ženy"

resultsViewM6km: Lang -> String
resultsViewM6km lang  =
  case lang of 
      En -> "Výsledky '21 - 6000m Muži"
      Sk -> "Výsledky '21 - 6000m Muži"

resultsViewW600: Lang -> String
resultsViewW600 lang  =
  case lang of 
      En -> "Výsledky '21 - MINIBeh 600m Dievčatá"
      Sk -> "Výsledky '21 - MINIBeh 600m Dievčatá"

resultsViewM600: Lang -> String
resultsViewM600 lang  =
  case lang of 
      En -> "Výsledky '21 - MINIBeh 600m Chlapci"
      Sk -> "Výsledky '21 - MINIBeh 600m Chlapci"

resultsViewWnw: Lang -> String
resultsViewWnw lang  =
  case lang of 
      En -> "Výsledky '21 - 6000m Nordic Walking Ženy"
      Sk -> "Výsledky '21 - 6000m Nordic Walking Ženy"

resultsViewMnw: Lang -> String
resultsViewMnw lang  =
  case lang of 
      En -> "Výsledky '21 - 6000m Nordic Walking Muži"
      Sk -> "Výsledky '21 - 6000m Nordic Walking Muži"

resultsViewPartners: Lang -> String
resultsViewPartners lang  =
  case lang of 
      En -> "Podporili nás"
      Sk -> "Podporili nás"

resultsViewPhotos: Lang -> String
resultsViewPhotos lang  =
  case lang of 
      En -> "Fotky"
      Sk -> "Fotky"

resultsViewFirstName: Lang -> String
resultsViewFirstName lang  =
  case lang of 
      En -> "Meno"
      Sk -> "Meno"

resultsViewLastname: Lang -> String
resultsViewLastname lang  =
  case lang of 
      En -> "Priezvisko"
      Sk -> "Priezvisko"

resultsViewStartNumber: Lang -> String
resultsViewStartNumber lang  =
  case lang of 
      En -> "Štartovné prvočíslo"
      Sk -> "Štartovné prvočíslo"

resultsViewTeam: Lang -> String
resultsViewTeam lang  =
  case lang of 
      En -> "Tím"
      Sk -> "Tím"

resultsViewTime: Lang -> String
resultsViewTime lang  =
  case lang of 
      En -> "Čas"
      Sk -> "Čas"

resultsViewLoss: Lang -> String
resultsViewLoss lang  =
  case lang of 
      En -> "Strata"
      Sk -> "Strata"

contacts: Lang -> String
contacts lang  =
  case lang of 
      En -> "Contact"
      Sk -> "Kontakt"

withLove: Lang -> String
withLove lang  =
  case lang of 
      En -> "Made with love by ManaBoost organization"
      Sk -> "S láskou pripravuje ManaBoost o.z."