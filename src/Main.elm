module Main exposing (main)

import Browser
import Html
import Html.Attributes
import Html.Events as Events
import Http
import Json.Encode as Encode
import List.Extra as List
import Maybe.Extra as Maybe
import RemoteData
import Results
import Translations


type Model
    = Model
        { lang : Translations.Lang
        , activeMap : Map
        , form : Form
        , currentView : View
        }


type alias Form =
    { firstName : Maybe String
    , lastName : Maybe String
    , email : Maybe String
    , team : Maybe String
    , startNumber : Maybe Int
    , disableButton : Bool
    , status : RemoteData.RemoteData Http.Error String
    }


type View
    = Index
    | Results


type Msg
    = ChangeMap Map
    | ChosenStrartNumber (Maybe Int)
    | ChosenFirstName String
    | ChosenLastName String
    | ChosenEmail String
    | ChosenTeam String
    | SubmitForm
    | SubmitedForm (Result Http.Error ())
    | GotoResults
    | GotoIndex
    | ChangeLang Translations.Lang


type Map
    = Map6
    | Map11


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }


init : () -> ( Model, Cmd msg )
init () =
    ( Model
        { lang = Translations.Sk
        , activeMap = Map6
        , form = emptyForm
        , currentView = Index
        }
    , Cmd.none
    )


emptyForm : Form
emptyForm =
    { firstName = Nothing
    , lastName = Nothing
    , email = Nothing
    , team = Nothing
    , startNumber = Nothing
    , disableButton = False
    , status = RemoteData.NotAsked
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model ({ form, lang } as model)) =
    case msg of
        ChangeMap map ->
            ( Model { model | activeMap = map }, Cmd.none )

        ChosenStrartNumber number ->
            let
                updatedForm =
                    { form | startNumber = number }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenFirstName firstName ->
            let
                updatedForm =
                    { form | firstName = Just firstName }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenLastName lastName ->
            let
                updatedForm =
                    { form | lastName = Just lastName }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenEmail email ->
            let
                updatedForm =
                    { form | email = Just email }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenTeam team ->
            let
                updatedForm =
                    { form | team = Just team }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        SubmitForm ->
            let
                updatedForm =
                    { form | disableButton = True }
            in
            ( Model { model | form = updatedForm }, submitForm model.form )

        SubmitedForm result ->
            case result of
                Ok () ->
                    let
                        updatedForm =
                            { emptyForm | status = RemoteData.Success (Translations.formSuccessMessage lang) }
                    in
                    ( Model { model | form = updatedForm }, Cmd.none )

                Err err ->
                    let
                        updatedForm =
                            { emptyForm | status = RemoteData.Failure err }
                    in
                    ( Model { model | form = updatedForm }, Cmd.none )

        GotoResults ->
            ( Model { model | currentView = Results }, Cmd.none )

        GotoIndex ->
            ( Model { model | currentView = Index }, Cmd.none )

        ChangeLang language ->
            ( Model { model | lang = language }, Cmd.none )


isFilled : Form -> Bool
isFilled form =
    List.all (\x -> x)
        [ Maybe.isJust form.firstName
        , Maybe.isJust form.lastName
        , Maybe.isJust form.email
        ]


baseApiUrl : String
baseApiUrl =
    "https://httpbin.org/anything/"


submitForm : Form -> Cmd Msg
submitForm form =
    if isFilled form then
        let
            body =
                Encode.object
                    [ ( "firstName", Encode.string <| Maybe.withDefault "" <| form.firstName )
                    , ( "lastName", Encode.string <| Maybe.withDefault "" <| form.lastName )
                    , ( "email", Encode.string <| Maybe.withDefault "" <| form.email )
                    , ( "team", Encode.string <| Maybe.withDefault "" <| form.team )
                    , ( "startNumber", Encode.int <| Maybe.withDefault 0 <| form.startNumber )
                    ]
        in
        Http.post
            { url = baseApiUrl ++ "register"
            , body = Http.jsonBody body
            , expect = Http.expectWhatever SubmitedForm
            }

    else
        Cmd.none


view : Model -> Browser.Document Msg
view ((Model { lang, currentView }) as model) =
    let
        view_ : List (Html.Html Msg)
        view_ =
            case currentView of
                Index ->
                    [ cover lang, mainContent model ]

                Results ->
                    [ Results.view lang ]
    in
    Browser.Document (Translations.title lang)
        ([ langMenu, navigation lang ] ++ view_ ++ [ contacts lang, Html.footer [] [ Html.text (Translations.withLove lang) ] ])


langMenu : Html.Html Msg
langMenu =
    Html.ul [ Html.Attributes.class "langs" ]
        [ Html.li [ Events.onClick <| ChangeLang Translations.Sk ] [ Html.text "SK" ]
        , Html.li [ Events.onClick <| ChangeLang Translations.En ] [ Html.text "EN" ]
        ]


navigation : Translations.Lang -> Html.Html Msg
navigation lang =
    Html.nav []
        [ Html.a [ Html.Attributes.id "menu-btn" ] [ Html.i [ Html.Attributes.class "fas fa-bars" ] [] ]
        , Html.ul []
            [ Html.li [] [ Html.a [ Html.Attributes.href "#domov", Events.onClick GotoIndex ] [ Html.text (Translations.navHome lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#o-behu", Events.onClick GotoIndex ] [ Html.text (Translations.navAbout lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#registracia", Events.onClick GotoIndex ] [ Html.text (Translations.navRegistration lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#zero-waste", Events.onClick GotoIndex ] [ Html.text (Translations.navZeroWaste lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#vysledky", Events.onClick GotoIndex ] [ Html.text (Translations.navResults lang) ] ]
            ]
        ]


contacts : Translations.Lang -> Html.Html msg
contacts lang =
    Html.div [ Html.Attributes.id "kontakt" ]
        [ Html.h2 [] [ Html.text (Translations.contacts lang) ]
        , Html.ul []
            [ Html.li [] [ Html.i [ Html.Attributes.class "fas fa-at" ] [], Html.text "info.malinovypohar@gmail.com" ]
            , Html.li [] [ Html.i [ Html.Attributes.class "fas fa-phone" ] [], Html.text "+421-917-915-445" ]
            , Html.li []
                [ Html.a
                    [ Html.Attributes.href "https://www.facebook.com/malinovypohar/" ]
                    [ Html.i [ Html.Attributes.class "fab fa-facebook" ] [], Html.text "facebook/malinovypohar" ]
                ]
            , Html.li []
                [ Html.a
                    [ Html.Attributes.href "https://www.instagram.com/malinovypohar/" ]
                    [ Html.i [ Html.Attributes.class "fab fa-instagram" ] [], Html.text "insta/malinovypohar" ]
                ]
            ]
        ]


cover : Translations.Lang -> Html.Html msg
cover lang =
    Html.section [ Html.Attributes.id "domov" ]
        [ Html.main_ []
            [ Html.img [ Html.Attributes.alt "MALINOVÝ POHÁR 11.9.2021", Html.Attributes.id "title", Html.Attributes.src "https://malinovypohar.sk/static/images/logo-title.svg" ]
                []
            ]
        ]


mainContent : Model -> Html.Html Msg
mainContent ((Model { lang, activeMap }) as model) =
    Html.main_ [ Html.Attributes.class "main" ]
        [ Html.section [ Html.Attributes.id "o-behu" ]
            [ Html.h1 []
                [ Html.text (Translations.mainAbout lang) ]
            , Html.p []
                [ Html.text (Translations.mainDate lang)
                , Html.b []
                    [ Html.text "11.9.2021" ]
                , Html.text "."
                ]
            , Html.p []
                [ Html.text (Translations.mainP1 lang) ]
            , Html.p []
                [ Html.text (Translations.mainP2 lang) ]
            , Html.p []
                [ Html.text (Translations.mainP3 lang) ]
            , Html.ul [ Html.Attributes.id "tracks" ]
                [ Html.li
                    [ Html.Attributes.classList [ ( "inactive", activeMap == Map11 ) ]
                    , Events.onClick <| ChangeMap Map6
                    ]
                    [ Html.a []
                        [ Html.text ("6km " ++ Translations.mainTrack lang) ]
                    ]
                , Html.li
                    [ Html.Attributes.classList [ ( "inactive", activeMap == Map6 ) ]
                    , Events.onClick <| ChangeMap Map11
                    ]
                    [ Html.a []
                        [ Html.text ("11km " ++ Translations.mainTrack lang) ]
                    ]
                ]
            , Html.img
                [ Html.Attributes.alt "Mapa"
                , Html.Attributes.class "map"
                , Html.Attributes.id "map-6"
                , Html.Attributes.src "https://malinovypohar.sk/static/images/map6.jpg"
                , Html.Attributes.classList [ ( "block", activeMap == Map6 ), ( "hidden", activeMap == Map11 ) ]
                ]
                []
            , Html.img
                [ Html.Attributes.alt "Mapa"
                , Html.Attributes.class "map"
                , Html.Attributes.id "map-11"
                , Html.Attributes.src "https://malinovypohar.sk/static/images/map11.jpg"
                , Html.Attributes.classList [ ( "block", activeMap == Map11 ), ( "hidden", activeMap == Map6 ) ]
                ]
                []
            ]
        , Html.section [ Html.Attributes.id "registracia" ]
            [ Html.h1 []
                [ Html.text (Translations.registrationTitle lang) ]
            , Html.p []
                [ Html.text (Translations.registrationIntro lang) ]
            , Html.p []
                [ Html.text (Translations.registrationSrdcia1 lang)
                , Html.a [ Html.Attributes.href "http://www.nadaciastastnesrdcia.sk/" ]
                    [ Html.text "Šťastné srdcia" ]
                , Html.text (Translations.registrationStdcia2 lang)
                ]
            , Html.p []
                [ Html.text (Translations.registrationPrimes1 lang)
                , Html.a [ Html.Attributes.href "https://en.wikipedia.org/wiki/List_of_prime_numbers#The_first_1000_prime_numbers" ]
                    [ Html.text (Translations.registrationPrimes2 lang) ]
                , Html.text (Translations.registrationPrimes3 lang)
                ]
            , reportRegistration model
            , registrationForm model
            ]
        , Html.section [ Html.Attributes.id "zero-waste" ]
            [ Html.h1 []
                [ Html.text "Zero Waste" ]
            , Html.p []
                [ Html.text (Translations.zeroWasteWhy lang) ]
            , Html.p []
                [ Html.text (Translations.zeroWasteMotivation lang) ]
            , Html.h2 []
                [ Html.text (Translations.zeroWasteStatsTitle lang) ]
            , Html.ul [ Html.Attributes.class "raspberry-list" ]
                [ Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteStatsP1 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteStatsP2 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteStatsP3 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteStatsP4 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteStatsP5 lang) ]
                    ]
                ]
            , Html.h2 []
                [ Html.text (Translations.zeroWasteTodoTitle lang) ]
            , Html.ul [ Html.Attributes.class "raspberry-list" ]
                [ Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteTodoP1 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteTodoP2 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteTodoP3 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteTodoP4 lang) ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text (Translations.zeroWasteTodoP5 lang) ]
                    ]
                ]
            ]
        , Html.section [ Html.Attributes.id "partners" ]
            [ Html.h1 []
                [ Html.text (Translations.partnersTitle lang) ]
            , Html.p []
                [ Html.text (Translations.partnersP1 lang)
                , Html.a [ Html.Attributes.href "#kontakt" ]
                    [ Html.text (Translations.partnersP2 lang) ]
                ]
            , Html.div [ Html.Attributes.id "sponsors" ]
                [ Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.viemiconsulting.sk/" ]
                        [ Html.img [ Html.Attributes.alt "VieMi Consulting", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/viemi.svg", Html.Attributes.title "VieMi Consulting" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.panex.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Panex", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/panex.png", Html.Attributes.title "Panex" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "http://www.nadaciastastnesrdcia.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Nadácia Šťastné Srdcia", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/stastne-srdcia.svg", Html.Attributes.title "Nadácia Šťastné Srdcia" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.idcreative.sk/" ]
                        [ Html.img [ Html.Attributes.alt "id creative", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/id-creative.jpg", Html.Attributes.title "id creative" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.ambulanciamalina.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Detská ambulancia Malina", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/malina.png", Html.Attributes.title "Detská ambulancia Malina" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.wiky.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Wiki", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/wiki.svg", Html.Attributes.title "Wiki" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://bratislavskykraj.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Bratislavský samosprávny kraj", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/bsk.svg", Html.Attributes.title "Bratislavský samosprávny kraj" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.zse.sk/" ]
                        [ Html.img [ Html.Attributes.alt "ZSE", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/zse.svg", Html.Attributes.title "ZSE" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "http://www.ovocnesnejky.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Ovocné SNEJKY", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/ovocnesnejky.png", Html.Attributes.title "Ovocné SNEJKY" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.muzeumpezinok.sk/sk" ]
                        [ Html.img [ Html.Attributes.alt "Malokarpatské múzeum v Pezinku", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/mmvp.jpg", Html.Attributes.title "Malokarpatské múzeum v Pezinku" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.toitoi.sk/" ]
                        [ Html.img [ Html.Attributes.alt "TOI TOI & DIXI", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/toitoi.png", Html.Attributes.title "TOI TOI & DIXI" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.dobryjezko.sk/" ]
                        [ Html.img [ Html.Attributes.alt "DOBRÝ JEŽKO", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/dobryjezko.svg", Html.Attributes.title "DOBRÝ JEŽKO" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.funmagazin.sk/rytmus-a-jasmina-sa-stahuju-takto-vyzera-ich-honosne-sidlo-za-580-000-eur-vo-vnutri/" ]
                        [ Html.img [ Html.Attributes.alt "Obec Malinovo", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/malinovo.png", Html.Attributes.title "Obec Malinovo" ]
                            []
                        ]
                    ]
                ]
            ]
        , Html.section [ Html.Attributes.id "vysledky" ]
            [ Html.h1 []
                [ Html.text (Translations.resultsTitle lang) ]
            , Html.div [ Html.Attributes.id "results" ]
                [ Html.a [ Events.onClick GotoResults ]
                    [ Html.text (Translations.resultsSubttile lang) ]
                ]
            , Html.h2 []
                [ Html.text (Translations.resultsRecords lang) ]
            , Html.div [ Html.Attributes.id "records" ]
                [ Html.div [ Html.Attributes.class "record" ]
                    [ Html.h3 []
                        [ Html.text (Translations.resultsWomanCategory lang) ]
                    , Html.b []
                        [ Html.text "Jeanette Borhy" ]
                    , Html.text (Translations.resultsWomanTime lang)
                    , Html.b []
                        [ Html.text "00:21:49.00" ]
                    , Html.text "!"
                    ]
                , Html.div []
                    [ Html.h3 []
                        [ Html.text (Translations.resultsManCategory lang) ]
                    , Html.b []
                        [ Html.text "Patrik Lopušný" ]
                    , Html.text (Translations.resultsManTime lang)
                    , Html.b []
                        [ Html.text "00:19:57.73" ]
                    , Html.text "!"
                    ]
                ]
            ]
        , Html.section [ Html.Attributes.id "o-nas" ]
            [ Html.h1 []
                [ Html.text (Translations.usTitle lang) ]
            , Html.div []
                [ Html.h2 []
                    [ Html.text "Heňo" ]
                , Html.img [ Html.Attributes.alt "Heňo", Html.Attributes.src "https://malinovypohar.sk/static/images/heno.jpg" ]
                    []
                , Html.p []
                    [ Html.text (Translations.usHeno lang) ]
                ]
            , Html.div []
                [ Html.h2 []
                    [ Html.text "Maťko" ]
                , Html.img [ Html.Attributes.alt "Maťko", Html.Attributes.src "https://malinovypohar.sk/static/images/matko.jpg" ]
                    []
                , Html.p []
                    [ Html.text (Translations.usMatus lang) ]
                ]
            ]
        ]


reportRegistration : Model -> Html.Html msg
reportRegistration ((Model { lang, form }) as model) =
    case form.status of
        RemoteData.Success msg ->
            Html.text msg

        RemoteData.Failure _ ->
            Html.text (Translations.formFailureMessage lang)

        _ ->
            Html.text ""


registrationForm : Model -> Html.Html Msg
registrationForm ((Model { lang, form }) as model) =
    Html.form
        [ Events.onSubmit SubmitForm ]
        [ Html.input
            [ Html.Attributes.id "first_name"
            , Html.Attributes.name "first_name"
            , Html.Attributes.placeholder (Translations.formFirstName lang)
            , Html.Attributes.attribute "required" ""
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.firstName)
            , Events.onInput ChosenFirstName
            ]
            []
        , Html.input
            [ Html.Attributes.id "last_name"
            , Html.Attributes.name "last_name"
            , Html.Attributes.placeholder (Translations.formLastname lang)
            , Html.Attributes.attribute "required" ""
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.lastName)
            , Events.onInput ChosenLastName
            ]
            []
        , Html.input
            [ Html.Attributes.id "email"
            , Html.Attributes.name "email"
            , Html.Attributes.placeholder "Email"
            , Html.Attributes.attribute "required" ""
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.email)
            , Events.onInput ChosenEmail
            ]
            []
        , Html.input
            [ Html.Attributes.id "team"
            , Html.Attributes.name "team"
            , Html.Attributes.placeholder (Translations.formTeam lang)
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.team)
            , Events.onInput ChosenTeam
            ]
            []
        , Html.input
            [ Html.Attributes.id "start_number"
            , Html.Attributes.name "start_number"
            , Html.Attributes.placeholder (Translations.formStartNumber lang)
            , Html.Attributes.type_ "text"
            , Html.Attributes.value
                (if form.startNumber == Nothing then
                    ""

                 else
                    String.fromInt <| Maybe.withDefault 0 form.startNumber
                )
            , Events.onInput (ChosenStrartNumber << String.toInt)
            ]
            []
        , Html.input
            [ Html.Attributes.id "submit"
            , Html.Attributes.name "submit"
            , Html.Attributes.type_ "submit"
            , Html.Attributes.value (Translations.formSubmit lang)
            , Html.Attributes.disabled <| form.disableButton || (not <| isFilled form)
            ]
            []
        ]
